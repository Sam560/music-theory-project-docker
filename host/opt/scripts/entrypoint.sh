#!/bin/bash

set -e

if [[ "$SET_PERMISSIONS" == "false" ]]; then
  chown -R nginx:nginx /var/www
  find /var/www -type d -print0|xargs -0 chmod 755; find /var/www -type f -print0|xargs -0 chmod 644;
fi

exec /usr/bin/supervisord -c /etc/supervisor/supervisor.conf --nodaemon