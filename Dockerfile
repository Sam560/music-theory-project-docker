FROM centos:7
MAINTAINER RADD Creative <devops@raddcreative.com>

RUN rpm -Uvh http://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/e/epel-release-7-12.noarch.rpm && \
    rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm && \
    yum update -y &&\
    yum clean all &&\
    yum-config-manager --disable remi-php54 &&\
    yum-config-manager --disable remi-php55 &&\
    yum-config-manager --disable remi-php56 &&\
    yum-config-manager --disable remi-php70 &&\
    yum-config-manager --disable remi-php71 &&\
    yum-config-manager --disable remi-php72 &&\
    yum-config-manager --disable remi-php56-debuginfo &&\
    yum-config-manager --disable remi-php55-debuginfo &&\
    yum-config-manager --enable remi-php73 &&\
    yum clean all &&\
    yum update &&\
    yum -y install \
    yum-utils \
    bash \
    cronie \
    mysql \
    rsync \
    vim \
    php73 \
    php73-php-bcmath \
    php73-php-cli \
    php73-php-common \
    php73-php-ctype \
    php73-php-curl \
    php73-php-fpm \
    php73-php-gd \
    php73-php-json \
    php73-php-iconv \
    php73-php-intl \
    php73-php-pdo_mysql \
    php73-php-mbstring \
    php73-php-mysqli \
    php73-php-opcache \
    php73-php-openssl \
    php73-php-pdo \
    php73-php-phar \
    php73-php-session \
    php73-php-xml \
    php73-php-posix \
    php73-php-zlib &&\
    ln -s /usr/bin/php73 /usr/bin/php && \
    ln -s /opt/remi/php73/root/usr/sbin/php-fpm /usr/bin/php-fpm

EXPOSE 8080

COPY ./host/ /

RUN yum -y install supervisor nginx wget nc &&\
    echo "installing phpmyadmin" &&\
    wget -qO- https://files.phpmyadmin.net/phpMyAdmin/4.8.3/phpMyAdmin-4.8.3-all-languages.tar.gz | tar xz -C /tmp &&\
    mkdir -p /var/www/phpmyadmin &&\
    rsync --delete --recursive /tmp/phpMyAdmin-4.8.3-all-languages/* /var/www/phpmyadmin &&\
    cp /opt/phpmyadmin/config.inc.php /var/www/phpmyadmin/config.inc.php

WORKDIR /var/www/

CMD ["/bin/bash", "/opt/scripts/entrypoint.sh"]