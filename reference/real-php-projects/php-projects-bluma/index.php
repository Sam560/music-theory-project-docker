<?php

//$link = mysqli_connect("localhost","chordchart_admin","password","chord_chart");
//
//    if(mysqli_connect_error()){
//        die("There was an issue with connecting to the database");
//    }
//
//    $query = "SELECT * FROM chords_in_chord_chart "; // make sure to use the table name
//
//    if ($result = mysqli_query($link, $query)) {
//      $row =  mysqli_fetch_array($result);
//
//      echo "Your chord choice is" .$row['A Major']. " and your chord values are".$row['A,C#,E']. "THanks a bunch";
//    }
//?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Chord Builder Project</title>
    <link rel="stylesheet" href="css/mystyles.css">
      <link rel="stylesheet" href="sass/mystyles.scss">
  </head>
  <body>
  <section class="hero is-primary is-danger ">
  <div class="hero-body">
      <div class="container">
          <h1 class="title">
              Chord Store and Finder Project
          </h1>
          <h2 class="subtitle">
              Purpose of this project is to store chords and reference them with php
          </h2>
      </div>
  </div>

  <div class="field">
      <div class="control">
          <input class="input" type="text" placeholder="Name a Key You would Like to Use">
          <br />
          <br />
          <a class="button is-primary is-large" >
           Submit The Chord
          </a>
      </div>
  </div>


      <?php
      require_once 'mandrill-api-php/src/Mandrill.php'; //Not required with Composer
      $mandrill = new Mandrill('YOUR_API_KEY');
      ?>



<!--     <h1 class="title">-->
<!--        How is this still working?-->
<!--      </h1>-->
<!---->
<!--      <p class="subtitle">-->
<!--Modern CSS framework based on <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox">Flexbox</a>-->
<!--      </p>-->
<!---->
<!--      <div class="field">-->
<!--        <div class="control">-->
<!--          <input class="input" type="text" placeholder="Input">-->
<!--        </div>-->
<!--      </div>-->
<!---->
<!--      <div class="field">-->
<!--        <p class="control">-->
<!--          <span class="select">-->
<!--            <select>-->
<!--              <option>Select dropdown</option>-->
<!--            </select>-->
<!--          </span>-->
<!--        </p>-->
<!--      </div>-->
<!---->
<!--      <div class="buttons">-->
<!--        <a class="button is-primary">Primary</a>-->
<!--        <a class="button is-link">Link</a>-->
<!--      </div>-->

      <footer class="footer">
          <div class="field">
              <div class="control">
                  <h2 class="subtitle">Email</h2>
                  <input class="input is-medium " type="text" placeholder="Email">
              </div>
              <br/>
              <div class="control">
                  <h2 class="subtitle">Subject</h2>
                  <input class="input is-medium" type="text" placeholder="Subject">
              </div>
          </div>
            <br />
          <h2 class="subtitle">Content</h2>
          <textarea class="textarea" placeholder="Subject Line Fool" rows="10"></textarea>
              <br/>
              <br/>
              <br/>
              <p>
                  <strong>Music Theory Application</strong> by <a href="https://www.schoenbergwebtools.com">Sam Schoenberg</a>. The source code is licensed
                  The website content is licensed <a href="https//www.schoenbergwebtools.com">CC BY NC SA 4.0</a>.
              </p>
          </div>
      </footer>
  </body>
</html>