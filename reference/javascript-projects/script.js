
// Major scale functionality

// target  all the major buttons
const majButtons = document.querySelectorAll('.majButton'); // comment later
// loop through the list
for (const majButton of majButtons) {
    // attach the onClick event
    majButton.addEventListener('click',displayScaleMaj);
}



// handle the onclick event
function displayScaleMaj() {

    //data to be displayed when button in major key is clicked
    const scalesMaj = {
        majKey1: 'C D E F G A B',
        majKey2: 'G A B C D E F#',
        majKey3: 'D E F# G A B C#',
        majKey4: 'A B C# D E F# G#',
        majKey5: 'E F# G# A B C# D#',
        majKey6: 'B C# D# E F# G# A#',
        majKey7: 'F# G# A# B C# D# E#',
        majKey8: 'C# D# E# F# G# A# B#',
        majKey9: 'F G A Bb C D E ',
        majKey10:' Bb C D Eb F G A ',
        majKey11:' Eb F G Ab Bb C D ',
        majKey12:' Ab Bb C Db Eb F G ',
        majKey13:' Db Eb F Gb Ab Bb C ',
        majKey14:' Gb Ab Bb Cb Db Eb F ',
        majKey15:'Cb Db Eb Fb Gb Ab Bb ',


    }

    // get scaleKeyMaj value from button that was clicked,
    const scaleKeyMaj = this.value;
    // get the scale that is asscoited with the specific value
    const scaleMaj = scalesMaj[scaleKeyMaj];
    // put the scale in the dome
    const userScaleMaj = document.getElementById('scale-area-maj');
    userScaleMaj.innerHTML = scaleMaj;
    console.log(scaleMaj);
};



// Minor Scale functionality

// target all the minor key buttons
    const minButtons =  document.querySelectorAll('.minButton');
// loop through the list
    for(const minButton of minButtons) {
        // attach the onClick event
        minButton.addEventListener('click',displayScaleMin);
    }

 //Handle the onClick event with minor keys
    function displayScaleMin() {
        // data to be displayed when button in minor key is clicked
        const scalesMin = {
            minKey1: 'C D Eb F G Ab Bb ',
            minKey2: 'G A Bb C D Eb ',
            minKey3: 'D E F G A Bb ',
            minKey4: 'A B C D E F G ',
            minKey5: 'E F# G A B C D ',
            minKey6: 'B C# D E F# G A ',
            minKey7: 'F# G# A B C# D E F',
            minKey8: 'C# D# E F# G# A B C',
            minKey9: 'F G Ab Bb C Db Eb ',
            minKey10:'Bb C Db Eb F Gb Ab,',
            minKey11:'Eb F Gb Ab Bb Cb Db E ',
            minKey12:'Ab Bb Cb Db Eb Fb Gb A ',
            minKey13:'Db Eb Fb Gb Ab A B  ',
            minKey14:'Gb Ab Bbb Cb Db Ebb F ',
            minKey15:'Cb Db D E F# G A  ',
        }
        // get scaleKeyMin value from the button that was clicked
            const scaleKeyMin = this.value;
        // const scaleKeyMin value from button that was clicked,
            const scaleMin = scalesMin[scaleKeyMin];
        // put the scale in the dome
            const userScaleMin = document.getElementById('scale-area-min');
            userScaleMin.innerHTML = scaleMin;
            console.log(scaleMin);

    };